FROM openjdk:10

COPY target/kubernetes-app-0.1.0-SNAPSHOT.jar /kubernetes-app.jar
EXPOSE 8080/tcp
ENTRYPOINT ["java", "-jar", "/kubernetes-app.jar", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap"]
