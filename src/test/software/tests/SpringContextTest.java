package software.tests;

import com.helloworld.KubernetesApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = KubernetesApp.class)
public class SpringContextTest {

	@Test
	public void contextLoads() {
	}

}
